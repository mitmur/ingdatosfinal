package pe.banco.dao;

import java.util.List;

import pe.banco.dto.UnidadSangre;

public interface UnidadesSangreDAO {
	public String ingresarUnidadSangre();

	public String modificarUnidadSangre();

	public UnidadesSangreDAO getUnidadSangre(Integer idUnidad);

	public List<UnidadSangre> listarUnidades(Integer idCentro, String tipo);

}
