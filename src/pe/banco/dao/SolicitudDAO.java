package pe.banco.dao;

import java.util.List;

import pe.banco.dto.Solicitud;

public interface SolicitudDAO {
	public void ingresarSolicitud(Solicitud sol);

	public void modificarSolicitud(Solicitud sol);

	public void eliminarSolicitud(Integer idSol);

	public Solicitud getSolicitud(Integer idSol);

	public List<Solicitud> listarSolicitud(Integer idCentroSalud);
}
