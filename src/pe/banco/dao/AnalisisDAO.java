package pe.banco.dao;

import pe.banco.dto.Analisis;

public interface AnalisisDAO {
	public void ingresarAnalisis(Analisis analisis);
}
