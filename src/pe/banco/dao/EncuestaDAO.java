package pe.banco.dao;

import pe.banco.dto.Encuesta;

public interface EncuestaDAO {
	public void ingresarEncuesta(Encuesta encuesta);
}
