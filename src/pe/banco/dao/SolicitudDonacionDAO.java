package pe.banco.dao;

import java.util.List;

import pe.banco.dto.Analisis;
import pe.banco.dto.Encuesta;
import pe.banco.dto.SolicitudDonacion;

public interface SolicitudDonacionDAO {
	public void ingresarSolicitudDon(SolicitudDonacion sold);

	public void modificarSolicitudDon(SolicitudDonacion sold);

	public void eliminarSolicitudDon(Integer id);

	public SolicitudDonacion getSolicitudDon(Integer id);

	public List<SolicitudDonacion> listarSolicitudDon(Integer idCentroMed);

}
