package pe.banco.dao;

import java.util.List;

import pe.banco.dto.Donante;

public interface DonanteDAO {
	public void ingresarDonante(Donante donante);

	public void modificarDonante(Donante donante);

	public void eliminarDonante(Donante donante);

	public Donante getDonante(String dnidon);

	public List<Donante> listarDonantes();
}
