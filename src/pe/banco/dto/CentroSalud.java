package pe.banco.dto;

import java.io.File;

public class CentroSalud {
	private Integer idcentrosalud;
	private String distrito;
	private String tipoCentro;
	private Integer telefono;
	private String direccion;
	private String nombreC;
	private File logo;
	public CentroSalud() {
		super();
	}
	public CentroSalud(Integer idcentrosalud, String distrito, String tipoCentro, Integer telefono, String direccion,
			String nombreC, File logo) {
		super();
		this.idcentrosalud = idcentrosalud;
		this.distrito = distrito;
		this.tipoCentro = tipoCentro;
		this.telefono = telefono;
		this.direccion = direccion;
		this.nombreC = nombreC;
		this.logo = logo;
	}
	public Integer getIdcentrosalud() {
		return idcentrosalud;
	}
	public void setIdcentrosalud(Integer idcentrosalud) {
		this.idcentrosalud = idcentrosalud;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getTipoCentro() {
		return tipoCentro;
	}
	public void setTipoCentro(String tipoCentro) {
		this.tipoCentro = tipoCentro;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNombreC() {
		return nombreC;
	}
	public void setNombreC(String nombreC) {
		this.nombreC = nombreC;
	}
	public File getLogo() {
		return logo;
	}
	public void setLogo(File logo) {
		this.logo = logo;
	}
	
	
}
