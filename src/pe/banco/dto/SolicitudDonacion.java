package pe.banco.dto;

public class SolicitudDonacion {
	private Integer idsolicitudDon;
	private Integer idencuesta;
	private Encuesta encuesta;
	private String dnidon;
	private Donante donante;
	private Integer idanalisis;
	private Analisis analisis;
	private Usuario usuario;
	private Integer idusuario;
	private String fechadon;
	private String dnipac;
	private Paciente paciente;

	public SolicitudDonacion() {
		super();
	}

	public SolicitudDonacion(Integer idsolicitudDon, Integer idencuesta, Encuesta encuesta, String dnidon,
			Donante donante, Integer idanalisis, Analisis analisis, Usuario usuario, Integer idusuario, String fechadon,
			String dnipac, Paciente paciente) {
		super();
		this.idsolicitudDon = idsolicitudDon;
		this.idencuesta = idencuesta;
		this.encuesta = encuesta;
		this.dnidon = dnidon;
		this.donante = donante;
		this.idanalisis = idanalisis;
		this.analisis = analisis;
		this.usuario = usuario;
		this.idusuario = idusuario;
		this.fechadon = fechadon;
		this.dnipac = dnipac;
		this.paciente = paciente;
	}

	public Integer getIdsolicitudDon() {
		return idsolicitudDon;
	}

	public void setIdsolicitudDon(Integer idsolicitudDon) {
		this.idsolicitudDon = idsolicitudDon;
	}

	public Integer getIdencuesta() {
		return idencuesta;
	}

	public void setIdencuesta(Integer idencuesta) {
		this.idencuesta = idencuesta;
	}

	public Encuesta getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

	public String getDnidon() {
		return dnidon;
	}

	public void setDnidon(String dnidon) {
		this.dnidon = dnidon;
	}

	public Donante getDonante() {
		return donante;
	}

	public void setDonante(Donante donante) {
		this.donante = donante;
	}

	public Integer getIdanalisis() {
		return idanalisis;
	}

	public void setIdanalisis(Integer idanalisis) {
		this.idanalisis = idanalisis;
	}

	public Analisis getAnalisis() {
		return analisis;
	}

	public void setAnalisis(Analisis analisis) {
		this.analisis = analisis;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}

	public String getFechadon() {
		return fechadon;
	}

	public void setFechadon(String fechadon) {
		this.fechadon = fechadon;
	}

	public String getDnipac() {
		return dnipac;
	}

	public void setDnipac(String dnipac) {
		this.dnipac = dnipac;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	
	
	

}
