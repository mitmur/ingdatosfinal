package pe.banco.dto;

public class Usuario {
private Integer idusuario;
private CentroSalud centroSalud;
private String tipoempleado;
private String nombreusuario;
private String password;
private String nombres;
private String apellidopat;
private String apellidomat;
private String direccion;
private Integer telefono;
private String correo;
public Usuario(Integer idusuario, CentroSalud centroSalud, String tipoempleado, String nombreusuario, String password,
		String nombres, String apellidopat, String apellidomat, String direccion, Integer telefono, String correo) {
	super();
	this.idusuario = idusuario;
	this.centroSalud = centroSalud;
	this.tipoempleado = tipoempleado;
	this.nombreusuario = nombreusuario;
	this.password = password;
	this.nombres = nombres;
	this.apellidopat = apellidopat;
	this.apellidomat = apellidomat;
	this.direccion = direccion;
	this.telefono = telefono;
	this.correo = correo;
}
public Usuario() {
	super();
}
public Integer getIdusuario() {
	return idusuario;
}
public void setIdusuario(Integer idusuario) {
	this.idusuario = idusuario;
}
public CentroSalud getCentroSalud() {
	return centroSalud;
}
public void setCentroSalud(CentroSalud centroSalud) {
	this.centroSalud = centroSalud;
}
public String getTipoempleado() {
	return tipoempleado;
}
public void setTipoempleado(String tipoempleado) {
	this.tipoempleado = tipoempleado;
}
public String getNombreusuario() {
	return nombreusuario;
}
public void setNombreusuario(String nombreusuario) {
	this.nombreusuario = nombreusuario;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getNombres() {
	return nombres;
}
public void setNombres(String nombres) {
	this.nombres = nombres;
}
public String getApellidopat() {
	return apellidopat;
}
public void setApellidopat(String apellidopat) {
	this.apellidopat = apellidopat;
}
public String getApellidomat() {
	return apellidomat;
}
public void setApellidomat(String apellidomat) {
	this.apellidomat = apellidomat;
}
public String getDireccion() {
	return direccion;
}
public void setDireccion(String direccion) {
	this.direccion = direccion;
}
public Integer getTelefono() {
	return telefono;
}
public void setTelefono(Integer telefono) {
	this.telefono = telefono;
}
public String getCorreo() {
	return correo;
}
public void setCorreo(String correo) {
	this.correo = correo;
}


}
