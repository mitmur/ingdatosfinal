package pe.banco.dto;

import java.io.File;
import java.sql.Blob;


public class Encuesta {
	private String dnidon;
	private String fecha;
	private Character estado;
	private byte [] pdfEncuesta;
	private String rutaEncuesta;

	public Encuesta() {
		super();
	}

	public Encuesta(String dnidon, String fecha, Character estado, byte[] pdfEncuesta, String rutaEncuesta) {
		super();
		this.dnidon = dnidon;
		this.fecha = fecha;
		this.estado = estado;
		this.pdfEncuesta = pdfEncuesta;
		this.rutaEncuesta = rutaEncuesta;
	}

	public String getDnidon() {
		return dnidon;
	}

	public void setDnidon(String dnidon) {
		this.dnidon = dnidon;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Character getEstado() {
		return estado;
	}

	public void setEstado(Character estado) {
		this.estado = estado;
	}

	public byte[] getPdfEncuesta() {
		return pdfEncuesta;
	}

	public void setPdfEncuesta(byte[] pdfEncuesta) {
		this.pdfEncuesta = pdfEncuesta;
	}

	public String getRutaEncuesta() {
		return rutaEncuesta;
	}

	public void setRutaEncuesta(String rutaEncuesta) {
		this.rutaEncuesta = rutaEncuesta;
	}

	

	

	

}
